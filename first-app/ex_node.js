// ============ MODULES ================ 
console.log(__filename);
console.log(__dirname);

const log = require('./logger');

log('message');
// ===================================



// =========== PATH MODULE ===============
const path = require('path');

var pathObj = path.parse(__filename);

console.log(pathObj);
// ======================================


// =========== OS MODULE ===============
const os = require('os');

var totalMemory = os.totalmem();
var freeMemory = os.freemem();

console.log(`Total memory: ${totalMemory}`);
console.log(`Total memory: ${freeMemory}`);
console.log(`Used memory: ${totalMemory - freeMemory}`);

// ======================================


// =========== FILE SYSTEM MODULE ===============
const fs = require('fs');

fs.readdir('./', (err, files) => {
  if(err) console.log('Error', err);
  else console.log('Result', files);
});
// ======================================

// =========== EVENTEMITTER SYSTEM MODULE ===============

const EventEmitter = require('events');

const emitter = new EventEmitter();

//Register a listener
emitter.on('messageLogged', () => {
  console.log('Listener called');
});


//Raised an event
emitter.emit('messageLogged');

// ==============================================

// =========== FILE SYSTEM MODULE ===============
// ==============================================

// =========== FILE SYSTEM MODULE ===============
// ==============================================