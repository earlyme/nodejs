const express = require('express');
const hbs = require('hbs');

var app = express();

app.use(express.static(__dirname + '/public'));


app.get('/', (req, res) => {
    res.render('home.hbs', {
        text1: 'Home',
        text2: 'Info',
        text3: 'Contacts',
        title: 'Home Webpage'
    });
});

app.set('view engine', 'hbs');

hbs.registerPartials(__dirname + '/views/partials');

app.get('/contacts', (req, res) => {
    res.render('contacts.hbs', {
        text1: 'Home',
        text2: 'Info',
        text3: 'Contacts',
        title: 'Contacts Webpage'
    });
});

app.listen(3000, () => {
    console.log('Listening to port 3000');
});